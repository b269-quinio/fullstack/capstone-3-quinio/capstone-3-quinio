const Product = require("../models/Product");



// Create Product (Admin only)
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.body.name,
			description : data.body.description,
			price: data.body.price
		});
		return newProduct.save().then((product, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Retrieve all ACTIVE products
module.exports.retrieveActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieve a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update Production Information (Admin Only)
module.exports.updateProduct = (data) => {
	if (data.isAdmin){
		let updatedProduct = {
			name: data.update.name,
			description: data.update.description,
			price: data.update.price
		};
		return Product.findByIdAndUpdate(data.product.productId, updatedProduct).then((update, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

// Archive Product (Admin Only)
module.exports.archiveProduct = (reqParams, data) => {
	if (data.isAdmin){
		let updatedArchive = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedArchive).then((update, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

// Controller for getting all products (Admin only)
module.exports.getAllProducts = () =>
{
	return Product.find().then(result => {
		return result;
	});
};

// Retrieving archived products
module.exports.getArchivedProducts = () =>
{
	return Product.find({isActive:false}).then(result => {
		return result;
	});
};

// Restoring archived products (Admin only)
module.exports.restoreProduct = (reqParams) =>
{
	return Product.findByIdAndUpdate(reqParams.productId, {isActive: true}).then((product,error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
}

