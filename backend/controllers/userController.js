const User = require("../models/User");

// bcrypt (password-hashing)
const bcrypt = require("bcrypt");

// Authentication
const auth = require("../auth");



// User Registration
module.exports.registerUser = (reqBody) => {
	// Double checking if user already exists 
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return false
			// Continue to register
		} else {
			let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
			});

			return newUser.save().then((user,error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
		};
	});
};


// User Authentication
module.exports.signInUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.produceAccessToken(result)
				}
			} else {
				return false;
			};
		};
	});
};

// Retrieve User Details

module.exports.getDetails = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Get all registered users
module.exports.getAll = (data) => {
	// if the decoded auth token "isAdmin" field is true, show all user
	if(data.isAdmin){
		return User.find().then((users, err) => {
			if(err){
				return "No user found"
			}else{
				return users;
			}
		});
	};

	// if auth key is not admin, return resolve promise
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
	
};

// Set user as Admin (Admin only)
module.exports.setAdmin = (reqParams, data) => {
	if (data.isAdmin){
		let updatedRole = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(reqParams.userId, updatedRole).then((course, error) => {
			if (error){
				return "User does not exist";
			} else {
				return `User now has Admin rights`;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};