const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");

const auth = require("../auth");




// Route of Non-admin User checkout (Create Order)
router.post("/create", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		body: req.body
	}

	orderController.createOrder(data).then(
		resultFromController => res.send(resultFromController));
});




// Route for retrieving an authenticated user's orders
router.get("/user/active", (req, res) => {
	const data = auth.decode(req.headers.authorization)

	orderController.getOrder(data).then(
		resultFromController => res.send(resultFromController));
});



// Route for retrieving all orders
router.get("/admin/dataOrders", (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderController.getAllOrders(data).then(
		resultFromController => res.send(resultFromController));
});

module.exports = router;