const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");



// Create Product (Admin access only)
router.post("/create", auth.verify, (req, res) => {
	const data = {
		body: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(
		resultFromController => res.send(resultFromController));
});

// Route for retrieving active products
router.get("/active", (req, res) => {
	productController.retrieveActive().then(
		resultFromController => res.send(resultFromController));
});



// Route for retrieving a single product
router.get("/specific/:productId", (req, res) => {
	productController.getProduct(req.params).then(
		resultFromController => res.send(resultFromController));
});

// Route for Updating Product Information (Admin Only)
router.put("/update/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.params,
		update: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.updateProduct(data).then(
		resultFromController => res.send(resultFromController));
});


// Route for Archive Product (Admin Only)
router.patch("/archive/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, data).then(
		resultFromController => res.send(resultFromController));
});

// Route for Retrieving all Products (Admin Only)
router.get("/all", auth.verify, (req,res) =>
{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving archive products
router.get("/archives/all", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getArchivedProducts(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/restore/:productId", auth.verify, (req,res) =>
{
	productController.restoreProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;