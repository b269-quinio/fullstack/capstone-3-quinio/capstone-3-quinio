const express = require("express");

// Router Instance
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/signIn", (req, res) => {
	userController.signInUser(req.body).then(
		resultFromController => res.send(resultFromController));
});


// Route for retrieve user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getDetails({userId: userData.id}).then(
		resultFromController => res.send(resultFromController));
});

// Route for getting all registered users
router.get("/allUsers", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	
	userController.getAll(data).then(resultFromController => res.send(resultFromController));
})

// Route for setting user as Admin (Admin only)
router.patch("/:userId/role", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAdmin(req.params, data).then(
		resultFromController => res.send(resultFromController));
});


module.exports = router;