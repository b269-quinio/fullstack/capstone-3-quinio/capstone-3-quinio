import {useState, useEffect} from 'react';

import ProductCard from './ProductCard';


export default function AllProducts() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))
		})
	}, [setProducts])


	return (
		<>
		  <h3 className="text-center">Product Catalogue</h3>
		  {products}
		</>
	)


};


