import Button from 'react-bootstrap/Button';
import { CartContext } from '../CartContext';
import { useContext } from 'react';
import { getProductData } from "../sampleOrder";

export default function CartProduct(data) {
	const cart = useContext(CartContext);
	const id = data.id;
	const quantity = data.quantity;
	const productData = getProductData(id);

	return (
		<>
			<h3>{productData.title}</h3>
			<p>{quantity} total</p>
			<p>${ (quantity * productData.price).toFixed(2) }</p>
			<Button size="sm" onClick={() => cart.deleteFromCart(id)}>Remove</Button>
			<hr></hr>
		</>

	)
}