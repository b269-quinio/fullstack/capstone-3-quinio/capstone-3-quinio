import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {Navigate} from 'react-router-dom';

import { Form, Button, Card } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function CreateProduct() {

    const {user} = useContext(UserContext);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [buttonActive, setButtonActive] = useState(false);

    useEffect(() => {
        if(name !== "" && description !== "" && price !== "") {
        setButtonActive(true);
    } else {
        setButtonActive(false);
        };
    }, [name, description, price]);

    function createProduct(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: 'POST',
            headers: {
                "Content-Type" : "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data === false) {
                Swal.fire({
                    title: "Access Denied!",
                    icon: "error",
                    text: "Only people with ADMINISTRATION rights can add products"
                })
             } else {
                Swal.fire({
                    title: "Product has been successfully created!",
                    icon: "success",
                    text: "Please proceed accordingly"
                })

                }
             });

        // Clear input fields
        setName("");
        setDescription("");
        setPrice(""); 
};


    return (
/*        (user.id !== null) ?
        <Navigate to="/products"/>
        :*/
        <div className="justify-content-center d-flex mt-5">
              <Card style={{ width: "50rem" }}>
                <Card.Body>
                  <Card.Title>Create Product</Card.Title>
                  <Form onSubmit={(e) => createProduct(e)}>

                    <Form.Group controlId="formProductName">
                      <Form.Label>Product Name</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Enter product name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        required
                      />
                    </Form.Group>

                    <Form.Group controlId="formProductDescription">
                      <Form.Label>Product Description</Form.Label>
                      <Form.Control
                        as="textarea"
                        placeholder="Enter product description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required
                      />
                    </Form.Group>

                    <Form.Group controlId="formProductPrice">
                      <Form.Label>Price</Form.Label>
                      <Form.Control
                        type="number"
                        placeholder="Enter product price"
                        value={price}
                        onChange={(e) => setPrice(e.target.value)}
                        required
                      />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                      Create Product
                    </Button>

                  </Form>
                </Card.Body>
              </Card>
            </div>
    )
}
