import {Row, Col} from 'react-bootstrap';
import { orderSample } from '../sampleOrder';
import OrderCard from '../components/OrderCard';

export default function Order() { 
	return (
		<>
			<h1 align="center" className="p-3">Welcome to the store!</h1>
			<Row xs={1} md={3} className="g-4">
				{orderSample.map((product, index) => (
					<Col align = "center" key={index}>
						<OrderCard product={product} />
					</Col>	
				))}
			</Row>
		</>
	)
}