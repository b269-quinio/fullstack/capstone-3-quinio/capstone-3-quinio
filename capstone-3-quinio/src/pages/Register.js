import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((email !== "" && password1 !== "" && password2 !== "") && password1 === password2) {
        setIsActive(true);
    } else {
        setIsActive(false);
        };
    }, [email, password1, password2]);

    function registerUser(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data === false) {
                Swal.fire({
                    title: "Duplicate Email found",
                    icon: "error",
                    text: "Please provide a different email."
                })
             } else {
                Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "You will be redirected to the Login page!"
                })

                    navigate("/login");

                }
             });

        // Clear input fields
        setEmail("");
        setPassword1("");
        setPassword2(""); 
};


    return (
        (user.id !== null) ?
        <Navigate to="/products"/>
        :
        <Form onSubmit={(e) => registerUser(e)}>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Please enter your registration details
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Please enter your registration details
                </Button>
            }

        </Form>
    )

}


