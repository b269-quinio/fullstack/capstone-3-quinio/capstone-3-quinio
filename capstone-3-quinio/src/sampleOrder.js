
const orderSample = [
	{
		id: "1",
		title: "Boysen Flat Latex",
		price: 750
	},

	{
		id: "2",
		title: "Boysen Semi-Gloss",
		price: 800
	},

	{
		id: "3",
		title: "Boysen QDE",
		price: 960
	}
];

function getProductData(id) {
	let productData = orderSample.find(product =>
		product.id === id);

	if (productData == undefined) {
		console.log("Product data does not exist for " + id);
		return undefined;
	}

	return productData;
}

export { orderSample, getProductData };